"""
Produce plots for training input variables.
"""

import pandas as pd
import matplotlib.pyplot as plt

# Import data
data = pd.read_csv('tt-data.csv', delimiter=',')
data = data.drop(axis=1, labels='Unnamed: 0')


# Separate the signal and background
signal = [1]
background = [0]

signal_mask = t_tag['labels'].isin(signal)
background_mask = t_tag['labels'].isin(background)

signal_df = t_tag[signal_mask]
background_df = t_tag[background_mask]

# Distribution plots for all variables:
for variable in signal_df:
    plt.hist(background_df[variable], color='red', bins=50, label='Background', histtype='step', linewidth=2)
    plt.hist(signal_df[variable], color='blue', bins=50, label='Signal', histtype='step', linewidth=2)
    plt.title('{0}'.format(variable))
    plt.ylabel('Entries/Bin [N]')
    plt.grid()
    plt.legend()
    plt.savefig('{0}_dist.png'.format(variable))
    plt.show()


# In[ ]:




