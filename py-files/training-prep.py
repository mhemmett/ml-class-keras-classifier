"""
Import training data, drop variables, split into data and labels for classifier training.
"""

import numpy as np
from numpy import savetxt
import pandas as pd



t_tag = pd.read_csv('tt-data.csv', delimiter=',')


t_tag = t_tag.drop(axis=1, labels='Unnamed: 0')


data = t_tag.drop(axis=1, labels=['labels'])
labels = t_tag['labels']

data = data.to_numpy()
labels = labels.to_numpy()


np.savetxt('tt-tag-inputs.csv', data, delimiter=',')
np.savetxt('tt-tag-labels.csv', labels, delimiter = ',')

