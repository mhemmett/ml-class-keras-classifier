"""
Scale the input data, train the model, test the model and produce performance plots.
"""

import tensorflow as tf
import numpy as np
from numpy import savetxt, loadtxt
import pandas as pd
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
from numpy import savetxt, loadtxt


# Import data
t_tag = pd.read_csv('tt-data.csv', delimiter=',')

# Copy the data 
t_tag_max_scaled = t_tag.copy() 

# Apply normalization techniques 
for column in t_tag_max_scaled.columns: 
    t_tag_max_scaled[column] = t_tag_max_scaled[column] / t_tag_max_scaled[column].abs().max() 


# Drop unnecessary columns
t_tag_max_scaled = t_tag_max_scaled.drop(axis=1, labels='Unnamed: 0')
data = t_tag_max_scaled.drop(axis=1, labels='labels')
data = data.drop(axis=1, labels='weights')


# Save data and labels for this model
data = data.to_numpy()

labels = t_tag['labels']

labels = labels.to_numpy()

np.savetxt('high-level-features-data.csv', data, delimiter=',')
np.savetxt('high-level-features-labels.csv', labels, delimiter=',')


# Prepare model for training:
x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(data, labels, test_size= 0.2, random_state= 42)

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.05),
  tf.keras.layers.Dense(1, activation='sigmoid')
])

predictions = model(x_train[:1]).numpy()
tf.nn.sigmoid(predictions).numpy()
loss_fn = tf.keras.losses.BinaryCrossentropy(from_logits=False)
loss_fn(y_train[:1], predictions).numpy()

model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

history = model.fit(x_train, y_train, epochs=10, batch_size=32, validation_split = 0.1)


# Performance Plots on Training
plt.plot(history.history['accuracy'], color='blue')
plt.plot(history.history['val_accuracy'], color='red')
plt.title('Model Accuracy')
plt.ylabel('Accuracy [%]')
plt.xlabel('Epoch')
plt.legend(['Train', 'Validation'], loc='lower right')
plt.grid()
plt.show()
# "Loss"
plt.plot(history.history['loss'], color='blue')
plt.plot(history.history['val_loss'], color='red')
plt.title('Model Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Validation'], loc='upper right')
plt.grid()
plt.show()


# Test the model, performance plots
model.evaluate(x_test,  y_test, verbose=2)

y_pred_keras = model.predict(x_test)

fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_test, y_pred_keras)


# Compare true signal and background vs. model probability
signal_output = []
background_output = []
for i in range(len(y_test)):
    if y_test[i] == 1:
        signal_output.append(y_pred_keras[i][0])
    else:
        background_output.append(y_pred_keras[i][0])


# Plot probabilities for signal, background
plt.hist(signal_output, bins=20, color='blue', label='Signal', histtype='step', linewidth=2)
plt.hist(background_output, bins=20, color='red', label='Background', histtype='step', linewidth=2)
plt.title('NN Classifier Output')
plt.xlabel('Class Probability')
plt.ylabel('Entries/Bin [N]')
plt.legend()
plt.show()


# Calc AUC
auc_keras = auc(fpr_keras, tpr_keras)

# Plot ROC
plt.plot(fpr_keras, tpr_keras, color='blue', label='AUC=0.948')
plt.legend()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Reciever-Operating Curve')
plt.grid()
plt.savefig('high-level-features-roc.png')
plt.show()

